# GitLab CI templates

## Lint Staged and MR

```yaml
include:
  - project: '1024town/ci-templates'
    file: '/lint-staged-mr.gitlab-ci.yml'
```
